// Befehlsbibliothek für das LCD
#include <LiquidCrystal.h>

// Initialisieren der Ports des LCD 
//    D5 (Datenbus)
//    D4 (Datenbus)
//    R/W (Read or Write)
//    RS (Bestimmung ob Datenbit als Befehl oder Zeichendaten interpretiert werden)
//    Vee (Kontrastregelung)
//    Vdd (Stromversorgung))                                                            
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);                                                

// Variable für Start der Auswahl des Strings
int stringStart  = 16;

// Variable für Ende der Auswahl des Strings                                                                
int stringLength =  0;                                                                

void setup()                                                                          
{
  // Groesse des Displays initialisieren
  lcd.begin(16, 2);                                                                   
}

void loop()                                                                           
{
  // Cursor steuern (1. Stelle, 1. Zeile) und Text ausgeben
  lcd.setCursor(0, 0);                                                                
  lcd.print("Projekt 3:");                                                            

  // Cursor steuern (1. Stelle, 2. Zeile) und Text mit Scroll-Funktion ausgeben
  lcd.setCursor(0, 1);
  lcd.print(scrollLcdLeft("Ich bin ein Text fuer das Scrolling-Projekt"));
  
  // 400ms warten
  delay(400); 
}

// Scroll-Funktion
String scrollLcdLeft(String displayString)
{
  // Variable, die zurueckgegeben wird
  String endString;

  // 16 Leerzeichen am Anfang und Ende des zusammegefassten Strings
  String scrollStringBegin = "                ";
  String scrollStringEnd   = "                ";

  // zusammengefasster String; 2x 16 Leerzeichen, dazwischen der gewuenschte Text
  String stringScroll = scrollStringBegin + displayString + scrollStringEnd;

  // zurueckgegebener String bestehend aus einem substring des zusammengefassten Strings mit den Auswahlvariablen
  endString = stringScroll.substring(stringStart, stringLength);
  
  // Erhoehung der Auswahlvariablen um 1
  stringStart++;
  stringLength++;

  // Abbruchbedingung - Grenze ist die Laenge des zusammengefassten Strings
  if(stringStart > stringScroll.length())
  {
    stringStart = 16;
    stringLength = 0;
  }

  // fertiger String wird zurückgegeben
  return endString;
}
